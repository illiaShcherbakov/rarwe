import Route from '@ember/routing/route';

export default Route.extend({
  resetController(controller) {
    controller.setProperties({
      isAddingSong: false,
      newSongTitle: '',
      rating: null
    });
  },

  actions: {
    didTransition() {
      let band = this.modelFor('bands.band');
      document.title = `${band.name} songs - Rock & Roll`;
    }
  }
});
