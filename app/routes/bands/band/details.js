import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    return this.modelFor('bands.band');
  },

  actions: {
    willTransition(transition) {
      let band = this.modelFor('bands.band');
      if (this.controller.isEditing) {
        let leave = window.confirm('Are you sure?');
        if (!leave) {
          transition.abort();
        } else {
          this.controller.set('isEditing', false);
          band.set('description', this.controller.oldDescription);
        }
      }
    },
    didTransition() {
      let band = this.modelFor('bands.band');
      document.title = `${band.name} songs - Rock & Roll`;
    }
  }
});
