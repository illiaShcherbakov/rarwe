import Controller from '@ember/controller';
import { empty, alias } from '@ember/object/computed';
import { observer } from '@ember/object';

export default Controller.extend({
  isAddingSong: false,
  newSongName: '',
  isAddSongButtonDisabled: empty('newSongName'),
  artist: alias('model.slug'),
  newSongRating: null,

  bandChanged: observer('artist', function() {
    this.set('newSongName', '');
    this.set('isAddingSong', false);
  }),

  actions: {
    addSong() {
      this.set('isAddingSong', true);
    },

    cancelAddSong() {
      this.set('isAddingSong', false);
    },

    async saveSong(e) {
      //Create new song
      e.preventDefault();
      let newSong = this.get('store').createRecord('song', {
        title: this.get('newSongName'),
        band: this.model,
        rating: this.newSongRating
      });
      await newSong.save();
      this.setProperties({
        newSongName: '',
        newSongRating: null
      });
    },

    updateRating(song, rating) {
      song.set('rating', song.rating === rating ? 0 : rating);
      return song.save();
    }
  }
});
