import Controller from '@ember/controller';

export default Controller.extend({
  isEditing: false,
  oldDescription: '',

  actions: {
    toggleIsEditing() {
      this.set('oldDescription', this.model.description);
      this.toggleProperty('isEditing');
    },
    edit() {
      this.set('isEditing', true);
    },
    async save() {
      let band = this.model;
      await band.save();
      this.set('isEditing', false);
    }
  }
});
