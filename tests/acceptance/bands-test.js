import { module, test } from 'qunit';
import { visit, click, fillIn } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';
import setupMirageTest from 'ember-cli-mirage/test-support/setup-mirage';

module('Acceptance | bands', function(hooks) {
  setupApplicationTest(hooks);
  setupMirageTest(hooks);

  test('List bands', async function(assert) {
    this.server.create('band', { name: 'Radiohead' });
    this.server.create('band', { name: 'Long Distance Calling' });

    await visit('/');

    let bandLinks = document.querySelectorAll('.rr-band-link');

    assert.equal(bandLinks.length, 2, 'All band links are rendered');
    assert.ok(
      bandLinks[0].textContent.includes('Radiohead'),
      'First band link containes the band name'
    );
    assert.ok(
      bandLinks[1].textContent.includes('Long Distance Calling'),
      'Second band link containes the band name'
    );
  });

  test('Create a band', async function(assert) {
    this.server.create('band', { name: 'Royal Blood' });

    await visit('/');
    await click('label');
    await fillIn('.rr-input', 'Caspian');
    await click('.rr-action-button');

    let bandLinks = document.querySelectorAll('.rr-band-link');

    assert.equal(
      bandLinks.length,
      2,
      'All band links are rendered includin the new one'
    );
    assert.ok(
      bandLinks[1].textContent.includes('Caspian'),
      'The new band link is rendered last'
    );
    assert.ok(
      document
        .querySelector('.rr-navbar-item > .active')
        .textContent.includes('Songs'),
      'The Song tab is active'
    );
  });
});
